<?php

namespace Drupal\batch_service_interface;

/**
 * Interface BatchServiceInterface.
 */
interface BatchServiceInterface {
  public static function generateBatch($data);
  public static function runTask($taskName, $parameters, &$context);
  public static function finishedBatch($success, $results, $operations);
  public function generateBatchJob($data);
  public function doFinishBatch($success, $results, $operations);
  public function doTask($taskName, $parameters, &$context);
}
