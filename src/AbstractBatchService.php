<?php

namespace Drupal\batch_service_interface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Psr\Log\LoggerInterface;
use Drupal\Core\StringTranslation\TranslationInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;

/**
 * Class AbstractBatchService.
 */
abstract class AbstractBatchService implements BatchServiceInterface, ContainerAwareInterface {
  use StringTranslationTrait;

  /**
   * Psr\Log\LoggerInterface definition.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * Must be set in child classes to be the service name so the service can
   * bootstrap itself.
   *
   * @var string
   */
  protected static $serviceName = '';

  /**
   * Constructs a new AbstractBatchService object.
   */
  public function __construct(LoggerChannelFactoryInterface $logger_factory, TranslationInterface $stringTranslation) {
    $loggerName = 'Batch Service: ' . static::$serviceName;
    $this->logger = $logger_factory->get($loggerName);
    $this->stringTranslation = $stringTranslation;
  }

  /**
   * {@inheritdoc}
   */
  public function setContainer(ContainerInterface $container = null)
  {
    $this->container = $container;
  }

  /**
   * Static function to generate a job. This bootstraps the service and calls a
   * a method on the service object to ease development of actual service.
   */
  public static function generateBatch($data) {
    /* @var  BatchServiceInterface */
    $service = static::bootstrapService();
    return $service->generateBatchJob($data);
  }

  /**
   * Bootstraps the batch job functions to leverage services cleaning from the
   * main Drupal container.
   * Implements callback_batch_operation().
   */
  public static function runTask($taskName, $parameters, &$context) {
    /* @var  BatchServiceInterface */
    $service = static::bootstrapService();
    $service->doTask($taskName, $parameters, $context);
  }

  /**
   * Implements callback_batch_operation().
   */
  public function doTask($taskName, $parameters, &$context) {
    $this->$taskName($parameters, $context);
  }

  /**
   * Implements callback_batch_finished().
   */
  public static function finishedBatch($success, $results, $operations) {
    $service = static::bootstrapService();
    $service->doFinishBatch($success, $results, $operations);
  }

  /**
   * Uses the Drupal container to generate an actual instance of the service.
   * TODO: Switch to a more generic symfony construct.
   */
  protected static function bootstrapService() {
    return \Drupal::service(static::$serviceName);
  }

  /**
   * Helper function to do the work of create a batch once the main list of
   * operations has been generated within generateBatch, this method will
   * convert the list to a Drupal batch array that relies on this service to
   * get the job done.
   *
   * Operations array can come in one of two forms:
   *  An associative array to function names in the processing class.
   *  An sequential array with each element being an associative array with function names.
   */
  protected function prepBatchArray($title, $initMessage, $rawOps) {

    $className = get_class($this);
    $ops = [];

    foreach ($rawOps as $key => $data) {

      if (method_exists($this, $key)) {
        $ops[] = [
          $className . '::runTask',
          [ $key, $data ],
        ];
      }
      else if(is_array($data)) {
        $task = key($data);
        $ops[] = [
          $className . '::runTask',
          [ $task, $data[$task] ],
        ];
      }
      else {
        $this->logger->notice('Skipping requested operation during prep. Invalid operation structure.');
      }
    }

    // Setup final batch array.
    $batch = [
      'title'    => $title,
      'init_message' => $initMessage,
      'operations'  => $ops,
      'finished' => $className . '::finishedBatch',
    ];

    return $batch;
  }

}
