<?php

namespace Drupal\batch_example;
use Drupal\node\Entity\Node;
use Drupal\batch_service_interface\AbstractBatchService;

/**
 * Class ExampleBatchService logs the name of nodes with id provided on form.
 */
class ExampleBatchService extends AbstractBatchService {

  /**
   * Must be set in child classes to be the service name so the service can
   * bootstrap itself.
   *
   * @var string
   */
  protected static $serviceName = 'batch_example.example_batch';

  /**
   * Data from the form as needed.
   */
  public function generateBatchJob($data) {
    $ops = [];
    for ($i = 0; $i < $data['message_count']; $i++ ) {
      $ops[] = [
        'logMessage' => ['MessageIndex' => $i + 1],
      ];
    }

    return $this->prepBatchArray($this->t('Logging Messages'), $this->t('Starting Batch Processing'), $ops);
  }

  public function logMessage($data, &$context) {

    $this->logger->info($this->getRandomMessage());

    if (!isset($context['results']['message_count'])) {
      $context['results']['message_count'] = 0;
    }
    $context['results']['message_count']++;

  }

  public function doFinishBatch($success, $results, $operations) {
    drupal_set_message($this->t('Logged %count quotes', ['%count' => $results['message_count']]));
  }

  public function getRandomMessage() {
    $messages = [
      '“I always wanted to be somebody, but now I realize I should have been more specific.” – Lily Tomlin',
      '“If at first you don’t succeed, then skydiving definitely isn’t for you.” – Steven Wright',
      '“I find television very educational. Every time someone turns it on, I go in the other room and read a book.” – Groucho Marx',
      '“All you need in this life is ignorance and confidence, and then success is sure.” – Mark Twain',
      '“If you don’t know where you are going, you might wind up someplace else.” – Yogi Berra ',
      '"There never was a child so lovely but his mother was glad to get him asleep.” – Ralph Waldo Emerson',
      '"It took me fifteen years to discover I had no talent for writing, but I couldn’t give it up because by then I was too famous.” – Robert Benchley',
      '“Luck is what you have left over after you give 100 percent.” – Langston Coleman',
      '“Opportunity does not knock, it presents itself when you beat down the door.” – Kyle Chandler',
      '“Don’t worry about the world coming to an end today. It is already tomorrow in Australia.” – Charles Schulz',
      '“I didn’t fail the test. I just found 100 ways to do it wrong.” – Benjamin Franklin',
      '“Life is a shipwreck but we must not forget tossing in the lifeboats.” – Voltaire',
      '“When I hear somebody sigh, Life is hard, I am always tempted to ask, ‘Compared to what?‘” – Sydney Harris',
      '“The elevator to success is out of order. You’ll have to use the stairs… one step at a time.” – Joe Girard',
    ];

    return $messages[array_rand($messages)];

  }

}
