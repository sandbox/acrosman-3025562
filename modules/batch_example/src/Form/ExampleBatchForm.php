<?php

namespace Drupal\batch_example\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\batch_example\ExampleBatchService;
use Drupal\batch_service_interface\BatchServiceInterface;

/**
 * Class ExampleBatchForm.
 */
class ExampleBatchForm extends FormBase {

  /**
   * Drupal\batch_example\ExampleBatchService definition.
   *
   * @var \Drupal\batch_example\ExampleBatchService
   */
  protected $exampleBatchService;

  /**
   * Constructs a new ExampleBatchForm object.
   */
  public function __construct(
    BatchServiceInterface $batch_service_interface
  ) {
    $this->exampleBatchService = $batch_service_interface;
  }

  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('batch_example.example_batch')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'example_batch_service_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['intro'] = [
      '#type' => 'markup',
      '#markup' => $this->t('This form records a provided number of random log messages'),
    ];
    $form['message_count'] = [
      '#type' => 'number',
      '#title' => $this->t('Number of message to record in the logs'),
      '#weight' => '0',
    ];
    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $data = [
      'message_count' => $form_state->getValue('message_count'),
    ];

    $batch = $this->exampleBatchService->generateBatchJob($data);
    batch_set($batch);
  }
}
