## Batch Service Interface

This is an early development version of a batch service interface that makes it
easier to develop batch jobs in D8 that leverage the power of services and other
Symfony provided nicities that are now at our disposal but not used by the D8
batch processes by default.
